commit 51db012c9408d0ae08ea5f6dd8e663fb3a5a5dfd
Author: Florian Weimer <fweimer@redhat.com>
Date:   Thu Jun 13 18:56:30 2024 +0200

    resolv: Track single-request fallback via _res._flags (bug 31476)
    
    This avoids changing _res.options, which inteferes with change
    detection as part of automatic reloading of /etc/resolv.conf.
    
    Reviewed-by: DJ Delorie <dj@redhat.com>
    (cherry picked from commit 868ab8923a2ec977faafec97ecafac0c3159c1b2)

diff --git a/resolv/res_send.c b/resolv/res_send.c
index 3a4a20684fcb9a63..9c77613f374e5469 100644
--- a/resolv/res_send.c
+++ b/resolv/res_send.c
@@ -947,9 +947,11 @@ send_dg(res_state statp,
 		seconds /= statp->nscount;
 	if (seconds <= 0)
 		seconds = 1;
-	bool single_request_reopen = (statp->options & RES_SNGLKUPREOP) != 0;
-	bool single_request = (((statp->options & RES_SNGLKUP) != 0)
-			       | single_request_reopen);
+	bool single_request_reopen = ((statp->options & RES_SNGLKUPREOP)
+				      || (statp->_flags & RES_F_SNGLKUPREOP));
+	bool single_request = ((statp->options & RES_SNGLKUP)
+			       || (statp->_flags & RES_F_SNGLKUP)
+			       || single_request_reopen);
 	int save_gotsomewhere = *gotsomewhere;
 
 	int retval;
@@ -1006,14 +1008,14 @@ send_dg(res_state statp,
 		       have received the first answer.  */
 		    if (!single_request)
 		      {
-			statp->options |= RES_SNGLKUP;
+			statp->_flags |= RES_F_SNGLKUP;
 			single_request = true;
 			*gotsomewhere = save_gotsomewhere;
 			goto retry;
 		      }
 		    else if (!single_request_reopen)
 		      {
-			statp->options |= RES_SNGLKUPREOP;
+			statp->_flags |= RES_F_SNGLKUPREOP;
 			single_request_reopen = true;
 			*gotsomewhere = save_gotsomewhere;
 			__res_iclose (statp, false);
diff --git a/resolv/resolv-internal.h b/resolv/resolv-internal.h
index 24b164f6b5df4c99..944af3ee7613b3c4 100644
--- a/resolv/resolv-internal.h
+++ b/resolv/resolv-internal.h
@@ -26,6 +26,8 @@
 #define RES_F_VC        0x00000001 /* Socket is TCP.  */
 #define RES_F_CONN      0x00000002 /* Socket is connected.  */
 #define RES_F_EDNS0ERR  0x00000004 /* EDNS0 caused errors.  */
+#define RES_F_SNGLKUP	0x00200000 /* Private version of RES_SNGLKUP.  */
+#define RES_F_SNGLKUPREOP 0x00400000 /* Private version of RES_SNGLKUPREOP.  */
 
 /* The structure HEADER is normally aligned on a word boundary.  In
    some code, we need to access this structure when it may be aligned
